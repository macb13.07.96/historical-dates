interface IEventsDataList {
    year: number;
    event: string;
}

export interface IEventsData {
    type: string;
    list: IEventsDataList[];
}

export interface IEventsState {
    deg: number;
    currentEventsCount: number;
    currentEvent: number;
    prevEvent: number;
}
