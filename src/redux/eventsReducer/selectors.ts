import { createSelector } from '@reduxjs/toolkit';

import { RootState } from '../store';

const baseSelector = createSelector(
    (state: RootState) => state,
    (state) => state.events
);

export const selectCurrentEventIndex = createSelector(baseSelector, (state) => state.currentEvent);

export const selectPrevEventIndex = createSelector(baseSelector, (state) => state.prevEvent);
