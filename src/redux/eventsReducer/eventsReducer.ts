import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import { IEventsState } from './types';

const initialStateEvents: IEventsState = {
    deg: 0,
    prevEvent: 0,
    currentEventsCount: 0,
    currentEvent: 0,
};

const eventsSlice = createSlice({
    name: 'events',
    initialState: initialStateEvents,
    reducers: {
        selectEvent(state, data: PayloadAction<number>) {
            state.prevEvent = state.currentEvent;
            state.currentEvent = data.payload;
        },
    },
});

export const { selectEvent } = eventsSlice.actions;
export const eventsReducer = eventsSlice.reducer;
