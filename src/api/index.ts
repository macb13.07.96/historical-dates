import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {IEventsData} from "../redux/reducers/types";

export const api = createApi({
    reducerPath: 'historicalDatesAPI',
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://65f5aa2341d90c1c5e09f206.mockapi.io'
    }),
    endpoints: (build) => ({
        getHistoricalDates: build.query<IEventsData[], void>({
            query: (state) => ({
                url: '/historical-dates',
                method: 'GET',
            })
        })
    })
})

export const {useGetHistoricalDatesQuery} = api
