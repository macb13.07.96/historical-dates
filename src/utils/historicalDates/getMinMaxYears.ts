import { IEventsData } from '../../redux/eventsReducer/types';

export interface IGetMinMaxYearsReturned {
    minYear: number;
    maxYear: number;
    prevMinYear: number;
    prevMaxYear: number;
}

export const getMinMaxYears = (
    response: IEventsData[] | undefined,
    currentIndex: number,
    prevEventIndex: number
): IGetMinMaxYearsReturned => {
    const list = response?.[currentIndex].list || [];
    let minYear = list[0]?.year ?? 0;
    let maxYear = list[0]?.year ?? 0;

    list.forEach((value) => {
        if (value.year < minYear) {
            minYear = value.year;
            return;
        }

        if (value.year > maxYear) {
            maxYear = value.year;
            return;
        }
    });

    let prevMinYear = 0;
    let prevMaxYear = 0;

    if (prevEventIndex === currentIndex) {
        prevMinYear = minYear;
        prevMaxYear = maxYear;
    }

    if (prevEventIndex !== currentIndex) {
        const prevEvent = response?.[prevEventIndex].list ?? [];

        prevMinYear = prevEvent[0]?.year;
        prevMaxYear = prevEvent[0]?.year;

        response?.[prevEventIndex].list.forEach((value) => {
            if (value.year < prevMinYear) {
                prevMinYear = value.year;
                return;
            }

            if (value.year > prevMaxYear) {
                prevMaxYear = value.year;
                return;
            }
        });
    }

    return {
        minYear,
        maxYear,
        prevMinYear,
        prevMaxYear,
    };
};
