export interface IValueCoordinates {
    left: string;
    top: string;
}

/**
 * Утилита, вычисляющая координаты расположения точек на кругу.
 */
export const calculationCoordinates = (index: number, length: number): IValueCoordinates => {
    const radius = 50;
    const arc = 2 * Math.PI * (1 / length);
    const angle = index * arc;
    const x = radius * Math.cos(angle);
    const y = radius * Math.sin(angle);

    return { left: 50 + x + '%', top: 50 + y + '%' };
};
