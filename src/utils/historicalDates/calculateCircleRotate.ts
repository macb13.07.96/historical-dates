import { CIRCLE_DEG_COUNT, CIRCLE_OFFSET_DEG } from '../../constants/historicalDates';

/**
 * Утилита, вычисляющая поворот круга.
 */
export const calculateCircleRotate = (eventsCount: number, currentEventIndex: number): number => {
    const gap = CIRCLE_DEG_COUNT / eventsCount;
    return -1 * gap * currentEventIndex + CIRCLE_OFFSET_DEG;
};
