export const addZero = (num: number): string => {
    if (num > 10) {
        return String(num);
    }

    if (num < -10) {
        return String(num);
    }

    if (num < 10 && num >= 0) {
        return `0${num}`;
    }

    if (num > -10 && num < 0) {
        return `-0${num}`;
    }

    return String(num);
};
