import styled from 'styled-components';

import { EColors } from '../../enums/colors';

export const StyledMainLayout = styled.main`
    & {
        display: flex;
        flex-direction: column;
        width: 100%;
        background-color: ${EColors.MAIN_BACKGROUND};
    }
`;
