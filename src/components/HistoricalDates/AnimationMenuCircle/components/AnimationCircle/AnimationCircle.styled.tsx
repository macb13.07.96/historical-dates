import styled from 'styled-components';

import { EColors } from '../../../../../enums/colors';

export const StyledAnimationCircleWrapper = styled.div`
    & {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: 100;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    @media screen and (max-width: 640px) {
        display: none;
    }
`;

export const StyledAnimationCircleMenu = styled.div<{ rotate: number }>`
    & {
        position: relative;
        width: 530px;
        height: 530px;
        border: 1px solid ${EColors.CIRCLE_BORDER};
        border-radius: 50%;
        align-self: center;
        transform: rotate(${({ rotate }) => rotate}deg);
        transition:
            transform 0.9s ease-in-out,
            width 0.33s ease-in-out,
            height 0.33s ease-in-out;
    }

    @media screen and (max-width: 1070px) {
        width: 380px;
        height: 380px;
    }

    @media screen and (max-width: 768px) {
        width: 330px;
        height: 330px;
    }
`;
