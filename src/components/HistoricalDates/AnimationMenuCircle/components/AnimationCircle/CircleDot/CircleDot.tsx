import React, { useEffect, useState } from 'react';

import { CIRCLE_OFFSET_DEG } from '../../../../../../constants/historicalDates';
import { IValueCoordinates, calculateCircleRotate } from '../../../../../../utils/historicalDates';
import { StyledCircleDot, StyledCircleDotBtn, StyledCircleDotSelected } from './CircleDot.styled';

interface IAnimationItemProps {
    index: number;
    coords: IValueCoordinates;
    text: string;
    eventsCount: number;
    currentEventIndex: number;
    onSelectEvent: (index: number) => void;
}

export const CircleDot: React.FC<IAnimationItemProps> = ({
    index,
    coords,
    text,
    eventsCount,
    currentEventIndex,
    onSelectEvent,
}) => {
    const [deg, setDeg] = useState(CIRCLE_OFFSET_DEG);

    const isActiveDot = currentEventIndex === index;
    const invertDeg = -1 * deg;
    const currentViewIndex = index + 1;

    useEffect(() => {
        setDeg(calculateCircleRotate(eventsCount, currentEventIndex));
    }, [eventsCount, currentEventIndex]);

    const handleSelect = () => onSelectEvent(index);

    return (
        <StyledCircleDot isActive={isActiveDot} {...coords}>
            <StyledCircleDotBtn rotate={invertDeg} onClick={handleSelect}>
                <StyledCircleDotSelected opacity={Number(isActiveDot)}>
                    {text}
                </StyledCircleDotSelected>
                {currentViewIndex}
            </StyledCircleDotBtn>
        </StyledCircleDot>
    );
};
