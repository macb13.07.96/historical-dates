import styled, { css } from 'styled-components';

import { EColors } from '../../../../../../enums/colors';

export const StyledCircleDot = styled.div<{ isActive?: boolean; left: string; top: string }>`
    & {
        position: absolute;
        top: ${({ top }) => top};
        left: ${({ left }) => left};
        transform: translate(-50%, -50%) scale(0.14);
        display: flex;
        align-items: center;
        justify-content: center;
        width: 56px;
        height: 56px;
        border: 1px solid ${EColors.CIRCLE_DOT_BORDER};
        border-radius: 50%;
        text-align: center;
        font-size: 20px;
        line-height: 30px;
        background-color: ${EColors.CIRCLE_DOT_BACKGROUND};
        transition:
            transform 0.6s ease-in-out,
            background-color 0.6s ease-in-out;

        ${({ isActive }) =>
            isActive &&
            css`
                transform: translate(-50%, -50%) scale(1);
                background-color: ${EColors.CIRCLE_DOT_ACTIVE_BACKGROUND};
            `}
    }

    &:hover {
        transform: translate(-50%, -50%) scale(1);
        background-color: ${EColors.CIRCLE_DOT_ACTIVE_BACKGROUND};
    }
`;

export const StyledCircleDotBtn = styled.button<{ rotate: number }>`
  & {
    font-family: 'PT Sans', 'sans-serif';
    font-size: 20px;
    position: relative;
    border: none;
    background-color: transparent;
    color: ${EColors.H1};
    width: 100%;
    height: 100%;
    cursor: pointer;
    transform: rotate(${({rotate}) => rotate}deg);
    transition: transform 0.9s;
  }

  &:hover {
  }
`;

export const StyledCircleDotSelected = styled.div<{ opacity: number }>`
  & {
    font-family: 'PT Sans', 'sans-serif';
    font-size: 20px;
    position: absolute;
    left: 150%;
    font-weight: 700;
    opacity: ${({opacity}) => opacity};
    transition: opacity 1.2s ease-in-out;
  }
`;
