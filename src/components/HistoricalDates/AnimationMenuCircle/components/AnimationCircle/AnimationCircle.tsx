import React, { useCallback, useEffect, useMemo, useState } from 'react';

import { CIRCLE_OFFSET_DEG } from '../../../../../constants/historicalDates';
import { selectEvent } from '../../../../../redux/eventsReducer/eventsReducer';
import { IEventsData } from '../../../../../redux/eventsReducer/types';
import { useAppDispatch } from '../../../../../redux/store';
import {
    IValueCoordinates,
    calculateCircleRotate,
    calculationCoordinates,
} from '../../../../../utils/historicalDates';
import { StyledAnimationCircleMenu, StyledAnimationCircleWrapper } from './AnimationCircle.styled';
import { CircleDot } from './CircleDot';

interface IAnimationCircleProps {
    currentEventIndex: number;
    data?: IEventsData[];
}

export const AnimationCircle: React.FC<IAnimationCircleProps> = ({ data, currentEventIndex }) => {
    const dispatch = useAppDispatch();
    const [deg, setDeg] = useState(CIRCLE_OFFSET_DEG);

    const eventsCount = data?.length ?? 1;

    const dataWithCoords: Array<IEventsData & { coords: IValueCoordinates; text: string }> =
        useMemo(() => {
            return (
                data?.map((item: IEventsData, index) => {
                    return {
                        ...item,
                        coords: calculationCoordinates(index, data?.length || 0),
                        text: item.type,
                    };
                }) ?? []
            );
        }, [data]);

    useEffect(() => {
        setDeg(calculateCircleRotate(eventsCount, currentEventIndex));
    }, [eventsCount, currentEventIndex]);

    const handleSelectEvent = useCallback(
        (index: number) => {
            dispatch(selectEvent(index));
        },
        [data]
    );

    return (
        <StyledAnimationCircleWrapper>
            <StyledAnimationCircleMenu rotate={deg}>
                {dataWithCoords.map((item, index) => (
                    <CircleDot
                        currentEventIndex={currentEventIndex}
                        key={item.type}
                        index={index}
                        coords={item.coords}
                        text={item.type}
                        eventsCount={eventsCount}
                        onSelectEvent={handleSelectEvent}
                    />
                ))}
            </StyledAnimationCircleMenu>
        </StyledAnimationCircleWrapper>
    );
};
