import React, { useEffect, useMemo, useRef } from 'react';

import gsap from 'gsap';
import { TextPlugin } from 'gsap/all';

import { useGetHistoricalDatesQuery } from '../../../../../api';
import {
    selectCurrentEventIndex,
    selectPrevEventIndex,
} from '../../../../../redux/eventsReducer/selectors';
import { IEventsData } from '../../../../../redux/eventsReducer/types';
import { useAppSelector } from '../../../../../redux/store';
import { getMinMaxYears } from '../../../../../utils/historicalDates';
import { StyledYearPeriod, StyledYearPeriodMax, StyledYearPeriodMin } from './YearPeriod.styled';

gsap.registerPlugin(TextPlugin);

interface IYearPeriodProps {
    data?: IEventsData[];
    currentEventIndex: number;
    prevEventIndex: number;
}

export const YearPeriod: React.FC<IYearPeriodProps> = ({
    data,
    prevEventIndex,
    currentEventIndex,
}) => {
    const minYearRef = useRef(null);
    const maxYearRef = useRef(null);

    const { minYear, maxYear, prevMinYear, prevMaxYear } = useMemo(
        () => getMinMaxYears(data, currentEventIndex, prevEventIndex),
        [data, currentEventIndex, prevEventIndex]
    );

    useEffect(() => {
        const animationStepYears = (
            ref: React.RefObject<HTMLElement>,
            fromYear: number,
            toYear: number
        ) => {
            const timeline = gsap.timeline({ defaults: { ease: 'none' } });
            const step = fromYear < toYear ? 1 : -1;
            for (let year = fromYear; step > 0 ? year <= toYear : year >= toYear; year += step) {
                timeline.to(ref.current, {
                    duration: 0.05,
                    text: String(year),
                });
            }
            return timeline;
        };
        const minYearTimeline = animationStepYears(minYearRef, prevMinYear, minYear);
        const maxYearTimeline = animationStepYears(maxYearRef, prevMaxYear, maxYear);

        gsap.timeline().add(minYearTimeline, 0).add(maxYearTimeline, 0);

        return () => {
            minYearTimeline.clear();
            maxYearTimeline.clear();
        };
    }, [minYear, maxYear, prevMinYear, prevMaxYear]);

    return (
        <StyledYearPeriod>
            <StyledYearPeriodMin ref={minYearRef} className={'year'}>
                {prevMinYear}
            </StyledYearPeriodMin>
            <StyledYearPeriodMax ref={maxYearRef} className={'year'}>
                {prevMaxYear}
            </StyledYearPeriodMax>
        </StyledYearPeriod>
    );
};
