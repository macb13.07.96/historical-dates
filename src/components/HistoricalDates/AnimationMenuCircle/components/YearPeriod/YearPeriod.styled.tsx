import styled from 'styled-components';

import { EColors } from '../../../../../enums/colors';

export const StyledYearPeriod = styled.div`
    & {
        display: flex;
        justify-content: center;
        align-items: center;
        gap: 60px;
        width: 100%;
    }

    .year {
        margin: 0;
        font-family: 'PT Sans', 'sans-serif';
        font-weight: 700;
        font-size: 200px;
        line-height: 200px;
        transition: font-size 0.33s ease-in-out;

        @media screen and (max-width: 1070px) {
            font-size: 130px;
        }

        @media screen and (max-width: 640px) {
            font-size: 56px;
            line-height: 72px;
        }
    }

    @media screen and (max-width: 1070px) {
        justify-content: space-between;
        gap: 0;
    }

    @media screen and (max-width: 640px) {
        margin-bottom: 50px;
    }

    @media screen and (max-width: 520px) {
        justify-content: center;
        gap: 20px;
        margin-bottom: 0;
    }
`;

export const StyledYearPeriodMin = styled.p`
    & {
        color: ${EColors.CIRCLE_MIN_YEAR_TEXT};
    }
`;

export const StyledYearPeriodMax = styled.p`
    & {
        color: ${EColors.CIRCLE_MAX_YEAR_TEXT};
    }
`;
