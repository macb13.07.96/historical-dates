import styled from 'styled-components';

export const StyledEventsInfoMobile = styled.div`
    & {
        display: none;
        width: 100%;
        padding: 30px 0;
        border-bottom: 1px solid #c7cdd9;
        color: #42567a;
        font-size: 20px;
        font-weight: 700;

        @media screen and (max-width: 640px) {
            display: block;
            margin-bottom: 30px;
        }

        @media screen and (max-width: 520px) {
            padding: 30px 0;
        }
    }
`;
