import styled from 'styled-components';

export const StyledHistoricalDatesCircle = styled.div`
    & {
        position: relative;
        min-height: 355px;
        display: flex;
        align-items: center;
        flex-direction: column;
        justify-content: center;
    }

    @media screen and (max-width: 1070px) {
        min-height: 460px;
    }
    @media screen and (max-width: 640px) {
        min-height: auto;
    }
`;
