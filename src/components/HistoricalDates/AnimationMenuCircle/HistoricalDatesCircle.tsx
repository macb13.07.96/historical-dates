import React from 'react';

import { useGetHistoricalDatesQuery } from '../../../api';
import {
    selectCurrentEventIndex,
    selectPrevEventIndex,
} from '../../../redux/eventsReducer/selectors';
import { useAppSelector } from '../../../redux/store';
import { StyledHistoricalDatesCircle } from './HistoricalDatesCircle.styled';
import { AnimationCircle, EventsInfoMobile, YearPeriod } from './components';

export const HistoricalDatesCircle = () => {
    const currentEventIndex = useAppSelector(selectCurrentEventIndex);
    const prevEventIndex = useAppSelector(selectPrevEventIndex);
    const { currentData } = useGetHistoricalDatesQuery();

    return (
        <StyledHistoricalDatesCircle>
            <AnimationCircle currentEventIndex={currentEventIndex} data={currentData} />
            <YearPeriod
                currentEventIndex={currentEventIndex}
                data={currentData}
                prevEventIndex={prevEventIndex}
            />
            <EventsInfoMobile>
                {!currentData || currentData[currentEventIndex].type}
            </EventsInfoMobile>
        </StyledHistoricalDatesCircle>
    );
};
