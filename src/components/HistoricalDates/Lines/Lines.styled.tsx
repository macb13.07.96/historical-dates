import styled from 'styled-components';

import { EColors } from '../../../enums/colors';

export const StyledLineVertical = styled.div`
    & {
        position: absolute;
        top: 0;
        left: 50%;
        transform: translateX(-50%);
        opacity: 0.1;
        height: 100%;
        width: 1px;
        background: ${EColors.H1};
    }

    @media screen and (max-width: 640px) {
        display: none;
    }
`;

export const StyledLineHorizontal = styled.div`
    & {
        position: absolute;
        top: 480px;
        left: 0;
        transform: translateY(-50%);
        opacity: 0.1;
        width: 100%;
        height: 1px;
        background: ${EColors.H1};
    }

    @media screen and (max-width: 640px) {
        display: none;
    }
`;
