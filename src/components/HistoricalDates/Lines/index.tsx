export {
    StyledLineVertical as HistoricalDatesVerticalLine,
    StyledLineHorizontal as HistoricalDatesHorizontalLine,
} from './Lines.styled';
