import styled, { css } from 'styled-components';

import { EColors } from '../../../enums/colors';

interface IStyledPaginationBtnProps {
    paginationRule: 'prev' | 'next';
}

export const StyledPaginationWrapper = styled.div`
    & {
        max-width: 120px;
        width: 100%;
        margin-bottom: 56px;
    }
    @media screen and (max-width: 640px) {
        order: 1;
        margin-bottom: 0;
        transform: translateY(-30px);
    }
`;

export const StyledPaginationCounter = styled.div`
  & {
    font-family: "PT Sans", "sans-serif";
    padding-left: 4px;
    margin-bottom: 20px;
    font-size: 14px;
    line-height: 18px;
    color: ${EColors.H1};
  }

  @media screen and (max-width: 640px) {
    margin-bottom: 10px;
  }
`;
export const StyledPaginationBtnWrapper = styled.div`
    & {
        display: flex;
        justify-content: space-between;
    }

    @media screen and (max-width: 640px) {
        max-width: 90px;
    }
`;

export const StyledPaginationBtn = styled.button<IStyledPaginationBtnProps>`
    & {
        width: 50px;
        height: 50px;
        border-radius: 50%;
        background-color: transparent;
        border: 1px solid rgba(66, 86, 122, 0.5);
        transition: background-color, 0.2s, opacity, 0.2s;
        cursor: pointer;

        ${({ paginationRule }) =>
            paginationRule === 'next' &&
            css`
                svg {
                    transform: rotate(180deg);
                }
            `}
        &:hover {
            background-color: #fffefe;
        }

        &:disabled {
            opacity: 0.2;
            cursor: not-allowed;
        }
    }

    @media screen and (max-width: 640px) {
        & {
            width: 40px;
            height: 40px;
        }
    }
`;
