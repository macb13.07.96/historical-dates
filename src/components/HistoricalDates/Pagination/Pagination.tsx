import React from 'react';

import { useGetHistoricalDatesQuery } from '../../../api';
import { selectEvent } from '../../../redux/eventsReducer/eventsReducer';
import { selectCurrentEventIndex } from '../../../redux/eventsReducer/selectors';
import { useAppDispatch, useAppSelector } from '../../../redux/store';
import { addZero } from '../../../utils/addZero';
import { PaginationIcon } from '../../Icons/PaginationIcon';
import {
    StyledPaginationBtn,
    StyledPaginationBtnWrapper,
    StyledPaginationCounter,
    StyledPaginationWrapper,
} from './Pagination.styled';

export const Pagination: React.FC = () => {
    const dispatch = useAppDispatch();
    const { currentData } = useGetHistoricalDatesQuery();
    const currentEventIndex = useAppSelector(selectCurrentEventIndex);

    const viewIndex = currentEventIndex + 1;
    const eventsCount = currentData?.length ?? 0;

    const handlePrev = () => {
        const prevIndex = currentEventIndex - 1;
        if (prevIndex < 0) return;

        dispatch(selectEvent(prevIndex));
    };

    const handleNext = () => {
        const nextIndex = currentEventIndex + 1;
        if (!currentData || nextIndex > eventsCount - 1) return;

        dispatch(selectEvent(nextIndex));
    };

    return (
        <StyledPaginationWrapper>
            <StyledPaginationCounter>
                {addZero(viewIndex)}/{addZero(eventsCount)}
            </StyledPaginationCounter>
            <StyledPaginationBtnWrapper>
                <StyledPaginationBtn
                    paginationRule={'prev'}
                    onClick={handlePrev}
                    disabled={currentEventIndex === 0}
                >
                    <PaginationIcon />
                </StyledPaginationBtn>
                <StyledPaginationBtn
                    paginationRule={'next'}
                    onClick={handleNext}
                    disabled={viewIndex === eventsCount}
                >
                    <PaginationIcon />
                </StyledPaginationBtn>
            </StyledPaginationBtnWrapper>
        </StyledPaginationWrapper>
    );
};
