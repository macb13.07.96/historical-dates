import React, { useEffect } from 'react';

import gsap from 'gsap';
import { Navigation, Pagination } from 'swiper/modules';

import { useGetHistoricalDatesQuery } from '../../../api';
import { selectCurrentEventIndex } from '../../../redux/eventsReducer/selectors';
import { useAppSelector } from '../../../redux/store';
import { SliderBtnIcon } from '../../Icons/SliderBtnIcon';
import {
    StyledSlideDescription,
    StyledSlideTitle,
    StyledSliderBtn,
    StyledSliderContainer,
    StyledSliderPagination,
    StyledSwiper,
    StyledSwiperSlide,
} from './Slider.styled';

export const Slider: React.FC = () => {
    const currentEventIndex = useAppSelector(selectCurrentEventIndex);
    const { sliderData } = useGetHistoricalDatesQuery(undefined, {
        selectFromResult(res) {
            const data = res.currentData?.[currentEventIndex];
            const newData = data ? [...data.list] : [];
            const sort = newData.sort((a, b) => a.year - b.year);
            return {
                ...res,
                sliderData: sort,
            };
        },
    });

    useEffect(() => {
        const timeline = gsap.timeline({ defaults: { duration: 1.2 } });
        timeline.fromTo('.swiper_root', { y: 40, opacity: 0 }, { opacity: 1, y: 0 });

        return () => {
            timeline.clear();
        };
    }, [currentEventIndex]);

    return (
        <StyledSliderContainer>
            <StyledSwiper
                className={'swiper_root'}
                wrapperClass={'wrapper'}
                spaceBetween={25}
                initialSlide={0}
                grabCursor
                slidesPerView={2}
                modules={[Navigation, Pagination]}
                navigation={{
                    nextEl: `.next`,
                    prevEl: `.prev`,
                }}
                pagination={{
                    el: `.pagination`,
                    clickable: true,
                    bulletClass: `bullet`,
                    bulletActiveClass: `activeBullet`,
                }}
                breakpoints={{
                    1070: {
                        slidesPerView: 3,
                        spaceBetween: 40,
                    },
                }}
            >
                {sliderData?.map((item) => {
                    return (
                        <StyledSwiperSlide>
                            <StyledSlideTitle>{item.year}</StyledSlideTitle>
                            <StyledSlideDescription>{item.event}</StyledSlideDescription>
                        </StyledSwiperSlide>
                    );
                })}
            </StyledSwiper>

            <StyledSliderPagination className={'pagination'} />

            <StyledSliderBtn paginationRule={'prev'} className={'prev'}>
                <SliderBtnIcon />
            </StyledSliderBtn>
            <StyledSliderBtn paginationRule={'next'} className={'next'}>
                <SliderBtnIcon />
            </StyledSliderBtn>
        </StyledSliderContainer>
    );
};
