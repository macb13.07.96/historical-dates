import styled, { css } from 'styled-components';
import { Swiper, SwiperSlide } from 'swiper/react';

import { EColors } from '../../../enums/colors';
import { StyledPaginationBtn } from '../Pagination/Pagination.styled';

export const StyledSliderContainer = styled.div`
    position: relative;
`;

export const StyledSwiper = styled(Swiper)`
    & {
        display: flex;
        overflow: hidden;

        @media (max-width: 640px) {
            .swiper-slide-next {
                opacity: 0.4;
            }

            .swiper-slide:last-child {
                opacity: 1;
            }
        }
    }

    .wrapper {
        display: flex;
    }
`;

export const StyledSwiperSlide = styled(SwiperSlide)`
    .slide {
        width: 320px;
        padding-top: 4px;
        display: flex;
        flex-direction: column;
    }
`;

export const StyledSlideTitle = styled.h2`
    margin: 0 0 15px 0;
    color: ${EColors.H2};
    font-weight: 400;
    font-family: 'Bebas Neue', 'sans-serif';
    font-size: 25px;

    @media screen and (max-width: 520px) {
        font-size: 16px;
        line-height: 19px;
    }
`;

export const StyledSlideDescription = styled.p`
    margin: 0;
    color: ${EColors.H1};
    font-family: 'PT Sans', 'sans-serif';

    @media screen and (max-width: 520px) {
        font-size: 14px;
        line-height: 20px;
    }
`;

export const StyledSliderPagination = styled.div`
    & {
        display: none;
        justify-content: center;
        padding: 40px 0 20px;
        gap: 10px;

        @media screen and (max-width: 768px) {
            display: flex;
        }

        @media screen and (max-width: 640px) {
            padding: 60px 0 0;
        }
    }

    .bullet {
        width: 10px;
        height: 10px;
        border-radius: 50%;
        background-color: ${EColors.H1};
        opacity: 0.4;

        @media screen and (max-width: 520px) {
            width: 6px;
            height: 6px;
        }
    }

    .activeBullet {
        opacity: 1;
    }
`;

export const StyledSliderBtn = styled(StyledPaginationBtn)`
    & {
        position: absolute;
        width: 40px;
        height: 40px;
        top: 50%;
        background-color: ${EColors.WHITE};
        border: 1px solid transparent;

        ${({ paginationRule }) =>
            paginationRule === 'next' &&
            css`
                right: -65px;

                svg {
                    transform: rotate(0deg);
                }

                @media screen and (max-width: 1700px) {
                    right: -55px;
                }
            `}
        ${({ paginationRule }) =>
            paginationRule === 'prev' &&
            css`
                left: -65px;

                svg {
                    transform: rotate(180deg);
                }

                @media screen and (max-width: 1700px) {
                    left: -55px;
                }
            `}
        &:disabled {
            opacity: 0;
        }
    }

    @media screen and (max-width: 768px) {
        display: none;
    }
`;
