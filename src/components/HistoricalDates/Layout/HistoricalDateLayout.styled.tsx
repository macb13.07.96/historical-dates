import styled from 'styled-components';

export const StyledHistoricalDateLayout = styled.div`
    & {
        position: relative;
        display: flex;
        flex-direction: column;
        margin: 0 auto;
        max-width: 1440px;
        height: 1080px;
        width: 100%;
    }

    @media screen and (max-width: 640px) {
        padding: 40px 20px 40px 20px;
        min-height: 100dvh;
        height: fit-content;
    }

    @media screen and (max-width: 420px) {
        padding: 59px 20px 13px;
    }
`;
