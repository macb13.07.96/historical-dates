import styled from 'styled-components';

export const StyledWrapper = styled.div`
    & {
        position: relative;
        height: 100%;
        width: 100%;
        border: 1px solid rgba(66, 86, 122, 0.1);

        @media screen and (max-width: 420px) {
            border: none;
        }
    }
`;
