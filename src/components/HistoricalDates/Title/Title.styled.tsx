import styled from 'styled-components';

import { EColors } from '../../../enums/colors';

export const StyledTitle = styled.h1`
    & {
      font-family: 'PT Sans', 'sans-serif';
        position: relative;
        margin: 0;
        max-width: 350px;
        color: ${EColors.H1};
        font-weight: 700;
        font-size: 56px;
        line-height: 67.2px;
        height: fit-content;
    }

    &::before {
        content: '';
        position: absolute;
        left: -80px;
        top: 0;
        right: 0;
        bottom: 0;
        width: 5px;
        height: 100%;
        background: linear-gradient(180deg, #3877ee -5%, #ef5da8 85%);

        @media screen and (max-width: 640px) {
            display: none;
        }
    }

    @media screen and (max-width: 1070px) {
        max-width: 50px;
        font-size: 40px;
        line-height: 40px;
    }

    @media screen and (max-width: 640px) {
        margin-bottom: 30px;
    }

    @media screen and (max-width: 420px) {
        font-size: 20px;
        line-height: 24px;
        margin-bottom: 56px;
    }
`;
