import React from 'react';

import { StyledTitle } from './Title.styled';

export function Title() {
    return <StyledTitle>Исторические даты</StyledTitle>;
}
