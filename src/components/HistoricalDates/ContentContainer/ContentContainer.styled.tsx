import styled from 'styled-components';

export const StyledContentContainer = styled.div`
    & {
        display: flex;
        flex-direction: column;
        width: 100%;
        height: 100%;
        padding: 170px 80px 104px 80px;
    }

    @media screen and (max-width: 420px) {
        padding: 0;
    }
`;
