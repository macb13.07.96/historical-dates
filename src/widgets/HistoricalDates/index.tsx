import React from 'react';

import { HistoricalDatesCircle } from '../../components/HistoricalDates/AnimationMenuCircle';
import { ContentContainer } from '../../components/HistoricalDates/ContentContainer';
import { HistoricalDateLayout } from '../../components/HistoricalDates/Layout';
import {
    HistoricalDatesHorizontalLine,
    HistoricalDatesVerticalLine,
} from '../../components/HistoricalDates/Lines';
import { HistoricalDatesPagination } from '../../components/HistoricalDates/Pagination';
import { HistoricalDatesSlider } from '../../components/HistoricalDates/Slider';
import { HistoricalDatesTitle } from '../../components/HistoricalDates/Title';
import { Wrapper } from '../../components/HistoricalDates/Wrapper';

export const HistoricalDatesWidget = () => {
    return (
        <HistoricalDateLayout>
            <Wrapper>
                <HistoricalDatesVerticalLine />
                <HistoricalDatesHorizontalLine />
                <ContentContainer>
                    <HistoricalDatesTitle />
                    <HistoricalDatesCircle />
                    <HistoricalDatesPagination />
                    <HistoricalDatesSlider />
                </ContentContainer>
            </Wrapper>
        </HistoricalDateLayout>
    );
};
