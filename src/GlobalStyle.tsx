import { createGlobalStyle, css } from 'styled-components';

export const GlobalStyles = createGlobalStyle(
    {},
    css`
        * {
            box-sizing: border-box;
        }

        body {
            margin: 0;
        }
    `
);
