import React from 'react';
import { Provider } from 'react-redux';

import { GlobalStyles } from './GlobalStyle';
import { MainLayout } from './components/MainLayout';
import { store } from './redux/store';
import { HistoricalDatesWidget } from './widgets/HistoricalDates';

export const App = () => {
    return (
        <Provider store={store}>
            <GlobalStyles />
            <MainLayout>
                <HistoricalDatesWidget />
            </MainLayout>
        </Provider>
    );
};
