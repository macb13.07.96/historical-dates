export enum EColors {
    MAIN_BACKGROUND = '#F4F5F9',
    CIRCLE_BORDER = 'rgba(66, 86, 122, .2)',
    CIRCLE_DOT_ACTIVE_BACKGROUND = '#f4f5f9',
    CIRCLE_DOT_BACKGROUND = '#42567a',
    CIRCLE_DOT_BORDER = 'rgba(62, 88, 128, 0.5)',
    CIRCLE_MIN_YEAR_TEXT = '#5d5fef',
    CIRCLE_MAX_YEAR_TEXT = '#ef5da8',
    H1 = '#42567a',
    H2 = '#3877ee',
    WHITE = '#fff',
}
