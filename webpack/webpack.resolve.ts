import path from 'path';

export const getWebpackResolve = () => ({
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
    modules: [path.resolve(process.cwd(), './node_modules')],
});
