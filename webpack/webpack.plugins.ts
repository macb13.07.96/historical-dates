import CopyWebpackPlugin from 'copy-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';

export const getWebpackPlugins = () => [
    new HtmlWebpackPlugin({
        template: path.resolve(process.cwd(), 'public', 'index.html'),
        favicon: './public/favicon.ico',
        hash: true,
        scriptLoading: 'defer',
        title: 'Исторические даты',
    }),
    new CopyWebpackPlugin({
        patterns: ['./public/robots.txt', './public/manifest.json'],
    }),
];
