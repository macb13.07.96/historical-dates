import { WebpackConfiguration } from 'webpack-cli';

export const getWebpackRules = (): WebpackConfiguration['module']['rules'] => {
    return [
        {
            test: /\.(ts|js)x?$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/preset-env',
                        '@babel/preset-react',
                        '@babel/preset-typescript',
                    ],
                },
            },
        },
    ];
};
